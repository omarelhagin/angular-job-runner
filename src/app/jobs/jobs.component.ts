import { Component, OnInit } from '@angular/core';
import { Job } from '../job';
import { JobService } from '../job.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  selectedJob: Job;
  jobs: Job[];
  jobsForm: FormGroup = new FormGroup({
    nameInput: new FormControl(null, Validators.required),
    execTimeInput: new FormControl(null, Validators.required)
  });
  executeForm: FormGroup = new FormGroup({
    nThreadsInput: new FormControl(null, Validators.required)
  });

  constructor(private jobService: JobService) { }

  ngOnInit() {
    this.getJobs();
  }

  onSelect(job: Job): void {
    this.selectedJob = job;
  }

  getJobs(): void {
    this.jobService.getJobs()
        .subscribe(jobs => this.jobs = jobs);
  }

  addJob(): void {
    const newJobName: string = this.jobsForm.get('nameInput').value;
    const newJobExecTime: number = this.jobsForm.get('execTimeInput').value;
    const newJob: Job = {
      name: newJobName,
      executionTime: newJobExecTime,
      isComplete: false
    };

    this.jobService.addJob(newJob).subscribe((receivedMessage) => {
      this.jobs.push(newJob);
      this.jobService.logMessage(receivedMessage['message']);
      this.jobsForm.reset();
    });
  }

  executeJobs(): void {
    this.jobService
        .executeJobs(this.executeForm.get('nThreadsInput').value)
        .subscribe((receivedMessage) => {
          this.jobService.logMessage(receivedMessage['message']);
        });
    this.executeForm.reset();
  }
}
