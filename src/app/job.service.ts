import { Injectable } from '@angular/core';
import { Job } from './job';
import { JOBS } from './mock-jobs';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JobService {

  private jobsUrl = 'http://localhost:9100';
  private httpOptionsForGET = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient,
    private messageService: MessageService) { }

  getJobs(): Observable<Job[]> {
    return this.http.get<Job[]>(this.jobsUrl, this.httpOptionsForGET)
    .pipe(
      tap(_ => this.logMessage('fetched jobs')),
      catchError(this.handleError('getJobs', []))
      );
  }

  /** GET job by id. Will 404 if id not found */
  getJob(id: number): Observable<Job> {
    const url = `${this.jobsUrl}/${id}`;
    return this.http.get<Job>(url).pipe(
      tap(_ => this.logMessage(`fetched job id=${id}`)),
      catchError(this.handleError<Job>(`getJob id=${id}`))
    );
  }

  /** PUT: add a job on the server */
  addJob (job: Job): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.post(this.jobsUrl, job, httpOptions).pipe(
      tap(_ => this.logMessage(`adding job name=${job.name}`)),
      catchError(this.handleError<any>('addJob'))
    );
  }

  /** PUT: update the job on the server */
  updateJob (job: Job): Observable<any> {
    return this.http.put(this.jobsUrl, job, this.httpOptionsForGET).pipe(
      tap(_ => this.logMessage(`updated job name=${job.name}`)),
      catchError(this.handleError<any>('updateJob'))
    );
  }

  executeJobs(nThreads: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    const nThreadsJson = '{ "nThreads": ' + nThreads + ' }';
    this.logMessage(`executing jobs`);

    return this.http.post(`${this.jobsUrl}/execute`, JSON.parse(nThreadsJson), httpOptions).pipe(
      // tap(_ => this.logMessage(`executing jobs`)),
      catchError(this.handleError<number>(`executeJobs`))
    );
  }

  logMessage(message: String) {
    this.messageService.add(`JobService: ${message}`);
  }

  /**
  * Handle Http operation that failed.
  * Let the app continue.
  * @param operation - name of the operation that failed
  * @param result - optional value to return as the observable result
  */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.logMessage(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
