export class Job {
    name: string;
    executionTime: number;
    isComplete: boolean;
}
