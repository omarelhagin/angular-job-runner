import { JobService } from './../job.service';
import { Component, OnInit, Input } from '@angular/core';
import { Job } from '../job';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css']
})
export class JobDetailComponent implements OnInit {

  @Input() job: Job;

  constructor(private jobService: JobService) { }

  ngOnInit() {
  }

  save(): void {
    this.jobService.updateJob(this.job).subscribe(() => (console.log()));
  }
}
