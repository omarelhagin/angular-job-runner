import { Job } from './job';

export const JOBS: Job[] = [
    {name: 'Job 11', executionTime: 1000, isComplete: false},
    {name: 'Job 12', executionTime: 1000, isComplete: false},
    {name: 'Job 13', executionTime: 1000, isComplete: false},
    {name: 'Job 14', executionTime: 1000, isComplete: false},
    {name: 'Job 15', executionTime: 1000, isComplete: false},
    {name: 'Job 16', executionTime: 1000, isComplete: false},
    {name: 'Job 17', executionTime: 1000, isComplete: false},
    {name: 'Job 18', executionTime: 1000, isComplete: false},
    {name: 'Job 19', executionTime: 1000, isComplete: false},
    {name: 'Job 20', executionTime: 1000, isComplete: false}
];
